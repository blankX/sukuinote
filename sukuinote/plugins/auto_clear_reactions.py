import asyncio
from pyrogram import Client, filters
from pyrogram.raw.functions.messages import ReadReactions
from .. import config, log_errors

# I fucking HATE reactions!
@Client.on_message(filters.outgoing & filters.edited)
@log_errors
async def auto_clear_reactions(client, message):
    if not config['config'].get('auto_clear_reactions') or not message.reactions:
        return
    while True:
        if (await client.send(ReadReactions(peer=await client.resolve_peer(message.chat.id)))).pts_count > 0:
            break
        await asyncio.sleep(1)
