from collections import defaultdict
from pyrogram import Client, filters
from .. import config, log_errors

if config['telegram'].get('use_test_servers'):
    messages_seen = defaultdict(lambda: defaultdict(set))

    @Client.on_message(~filters.scheduled & ~filters.forwarded & ~filters.sticker & ~filters.via_bot & ~filters.edited & filters.me, group=-5)
    @log_errors
    async def test_server_deduper(client, message):
        try:
            messages_seen[client][message.chat.id].remove(message.message_id)
        except KeyError:
            messages_seen[client][message.chat.id].add(message.message_id)
        else:
            message.stop_propagation()
